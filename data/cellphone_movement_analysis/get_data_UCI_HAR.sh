mkdir data_uci
cd data_uci
wget https://archive.ics.uci.edu/ml/machine-learning-databases/00240/UCI%20HAR%20Dataset.zip
unzip UCI\ HAR\ Dataset.zip
mv UCI\ HAR\ Dataset\/* .
rm -rf UCI\ HAR\ Dataset
rm -rf UCI\ HAR\ Dataset.zip
cd ..
