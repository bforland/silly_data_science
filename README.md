# RunTracker Summary
This is a simple running perfromance tracker to show changes in performance over time. It is a simple custom metric that I developed. Originally done in [Google Sheets](https://docs.google.com/spreadsheets/d/1m5LLIAslumVh373jqSDHqd-VJMvwIGQIwVufwCByA80/edit?usp=sharing), I moved it to python for more functionality.

![alt text1][logo]

[logo]: img/performance.png "Performance"

### Metric
```math
\frac{A}{TempScale}\cdot\frac{155 * 8.5}{HR * Pace}
```
Where _HR_ is my average Heart Rate during the run, _Pace_ is my average minutes per mile, and _TempScale_ is a scale factor bases on the effect that external temperature has on running performance. I caculated a fit from the date [here](https://racereadycoaching.com/temperature-affects-running-performance/) to generate the scale. The metric has been normalized to a desired performance level of 8:30min per mile at a heart rate of 155 which is represented here by _A_. Historical temperature data was taken from [here](https://www.accuweather.com/en/fr/saint-genis-pouilly/161761/march-weather/161761)
