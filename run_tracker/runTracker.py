import pandas
import numpy as np
import argparse

class data:
    
    def __init__(self):
        
        temps = [-3.76, 1.24, 6.24, 11.24, 16.24, 21.24, 26.24]
        percn = [0.9623, 0.9906, 1.0, 0.9905, 0.9609, 0.9074, 0.8227]
    
        self.fit = np.polyfit(temps,percn,2)
        
        self.data = pandas.read_csv('data/data.csv',delimiter=',')
    
    def get_fit(self):

        return self.fit

    def calc2(self,hr,pace,temp):
        
        return np.round((self.fit[2] + self.fit[1] * temp + self.fit[0] * temp * temp) / (hr * pace), 5)

    def calc(self,hr,pace,temp):
        
        return np.round((1./1.02708)*(155. * 8.5)/(hr * pace) * 1. / (self.fit[2] + self.fit[1] * temp + self.fit[0] * temp * temp), 2)
    
    def trendline(self) -> (list):
        
        trend_con = np.polyfit(np.arange(len(self.data['Date'])),self.data['Performance'],1)
        
        trend_line = lambda x: trend_con[1] + trend_con[0] * x
        
        data_length = self.data.index[-1] + 1
        
        return trend_line(np.arange(data_length))

    def pace_to_float(self):

        pace_list = []
        
        for p in self.data['Pace']:

            pace_list.append(float(p.split(':')[0]) + float(p.split(':')[1])/60.)
        
        return pace_list
    
    def add_entry(self, date: str,
                        hr: float,
                        pace: str,
                        temp: float):
        
        pace_F = float(pace.split(':')[0]) + float(pace.split(':')[1])/60.
        
        performance = self.calc(hr, pace_F, temp)

        self.data = self.data.append({'Date': date, 'HR': hr, 'Pace': pace, 'Temp': temp, 'Performance': performance},ignore_index=True)

        print(self.data)
        
        self.save()

    def remove_entry(self, entry: int):
        
        self.data = self.data.drop(entry)

        print(self.data)
        
        self.save()
        
    def save(self):
        
        self.data.to_csv('data/data.csv',index=False)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Process running performance inputs.")
    parser.add_argument('-a', '--add', type=bool, default=False)
    parser.add_argument('-r', '--remove', type=bool, default=False)
    parser.add_argument('-v', '--view', type=bool, default=True)

    running_data = data()

    if parser.parse_args().view:

        print(running_data.data)

    if parser.parse_args().add:
        date = input("Enter date of run [dd/mm]: ")
        HR = input("Enter heart rate during run: ")
        pace = input("Enter average min per mile during run [HH:MM]: ")
        temp = input("Enter daily max temperature [C]: ")
        running_data.add_entry(date, float(HR), pace, float(temp))
    
    if parser.parse_args().remove:
        
        running_data.remove_entry(parser.parse_args().entry)